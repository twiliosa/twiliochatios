//
//  LoginController.swift
//  IPMQuickstart
//
//  Created by Peter Tan on 12/14/15.
//  Copyright © 2015 Twilio. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var warningMessage: UILabel!
    
    @IBAction func Login(sender: AnyObject) {
        if userName.text == "" {
            print("Empty Button")
            self.warningMessage.text = "Please Enter a Username"
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "loginToChat" {
            let nav = segue.destinationViewController as! UINavigationController
            let vc = nav.topViewController as! ChatViewController
            vc.username = userName.text! as String           
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.becomeFirstResponder()
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Dismiss keyboard if container view is tapped
    @IBAction func viewTapped(sender: AnyObject) {
        self.userName.resignFirstResponder()
    }
}

